---
title: 
date: 2019-09-29T23:50:15+08:00
---

Shangyan Zhou(Chinese: 周尚彦, Id: Sphish)

[Curriculum Vitae](https://1drv.ms/b/s!AtkmsXFdxiiQm3Bq-PUG70bxazuQ?e=AyJo1C)

### Education

- B.Science in Computer Science and Technology @ [Peking University](https://www.pku.edu.cn)

### Experience

- Software Developer Intern @ [Accutar Biotech](https://www.accutarbio.com/)
- Software Engineer Intern @ [Google](https://www.google.com)
- Teaching Assistant @ [Peking University](https://www.pku.edu.cn)
- Software Developer Intern @ [Hulu](https://www.hulu.com)
- Research Intern @ [ISCAS](https://iscas.ac.cn)

### Activities & Honors

- Gold Medal @ Asia-Pacific Informatics Olympiad 2015(Chinese District)
- Silver Medal @ National Olympiad in Informatics 2015
- Second Prize @ Peking University Programming Contest [2017, 2018]
- 3rd Place @ Jane Street Electronic Trading Contest 2018(Tsinghua Univ.)
