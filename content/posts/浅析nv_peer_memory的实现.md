---
title: "浅析nv_peer_memory的实现"
date: 2021-11-29T23:35:16+08:00
draft: false
tags: ["NVIDIA", "RDMA", "InfiniBand", "Linux Kernel"]
categories: ["代码阅读"]
---

[nv_peer_memory](https://github.com/Mellanox/nv_peer_memory) 是在Linux用户空间使用GPU Direct RDMA需要安装的一个kernel module。只要装上它之后，开发者就可以直接在Linux用户态 `ibv_reg_mr`一块显存，然后拿着这块显存直接去做RDMA，而不需要经过内存。


<!--more-->

听起来，nv_peer_memory要实现如此fancy的功能，实现一定很复杂。但实际上，最复杂的部分dma mapping的接口是由nvidia驱动提供的，nv_peer_mem这个模块本身做的事情并不多，只是借助了IB Peer Direct的技术，使得用户态能够方便地调用驱动提供的接口对显存做dma mapping。

## Peer Direct

PeerDirect 技术主要用于在用户态里直接让Mellanox IB网卡直接传数据到其他PCIe设备里。
拿nv_peer_mem来说，安装了这个module以后，在用户态里可以直接将显存register给IB卡，就可以直接对显存做RDMA操作。

使用PeerDirect需要实现一个kernel driver，在kernel中给ib_core 注册一个peer_memory_client（使用ib_register_peer_memory_client)。

peer_memory_client相关的定义在mlnx-ofed-kernel/include/peer_mem.h里

```c++
/**
 *  struct peer_memory_client - registration information for user virtual
 *                              memory handlers
 *
 * The peer_memory_client scheme allows a driver to register with the ib_umem
 * system that it has the ability to understand user virtual address ranges
 * that are not compatible with get_user_pages(). For instance VMAs created
 * with io_remap_pfn_range(), or other driver special VMA.
 *
 * For ranges the interface understands it can provide a DMA mapped sg_table
 * for use by the ib_umem, allowing user virtual ranges that cannot be
 * supported by get_user_pages() to be used as umems.
 */
struct peer_memory_client {
	char name[IB_PEER_MEMORY_NAME_MAX];
	char version[IB_PEER_MEMORY_VER_MAX];

	/**
	 * acquire - Begin working with a user space virtual address range
	 *
	 * @addr - Virtual address to be checked whether belongs to peer.
	 * @size - Length of the virtual memory area starting at addr.
	 * @peer_mem_private_data - Obsolete, always NULL
	 * @peer_mem_name - Obsolete, always NULL
	 * @client_context - Returns an opaque value for this acquire use in
	 *                   other APIs
	 *
	 * Returns 1 if the peer_memory_client supports the entire virtual
	 * address range, 0 or -ERRNO otherwise. If 1 is returned then
	 * release() will be called to release the acquire().
	 */
	int (*acquire)(unsigned long addr, size_t size,
		       void *peer_mem_private_data, char *peer_mem_name,
		       void **client_context);
	/**
	 * get_pages - Fill in the first part of a sg_table for a virtual
	 *             address range
	 *
	 * @addr - Virtual address to be checked whether belongs to peer.
	 * @size - Length of the virtual memory area starting at addr.
	 * @write - Always 1
	 * @force - 1 if write is required
	 * @sg_head - Obsolete, always NULL
	 * @client_context - Value returned by acquire()
	 * @core_context - Value to be passed to invalidate_peer_memory for
	 *                 this get
	 *
	 * addr/size are passed as the raw virtual address range requested by
	 * the user, it is not aligned to any page size. get_pages() is always
	 * followed by dma_map().
	 *
	 * Upon return the caller can call the invalidate_callback().
	 *
	 * Returns 0 on success, -ERRNO on failure. After success put_pages()
	 * will be called to return the pages.
	 */
	int (*get_pages)(unsigned long addr, size_t size, int write, int force,
			 struct sg_table *sg_head, void *client_context,
			 u64 core_context);
	/**
	 * dma_map - Create a DMA mapped sg_table
	 *
	 * @sg_head - The sg_table to allocate
	 * @client_context - Value returned by acquire()
	 * @dma_device - The device that will be doing DMA from these addresses
	 * @dmasync - Obsolete, always 0
	 * @nmap - Returns the number of dma mapped entries in the sg_head
	 *
	 * Must be called after get_pages(). This must fill in the sg_head with
	 * DMA mapped SGLs for dma_device. Each SGL start and end must meet a
	 * minimum alignment of at least PAGE_SIZE, though individual sgls can
	 * be multiples of PAGE_SIZE, in any mixture. Since the user virtual
	 * address/size are not page aligned, the implementation must increase
	 * it to the logical alignment when building the SGLs.
	 *
	 * Returns 0 on success, -ERRNO on failure. After success dma_unmap()
	 * will be called to unmap the pages. On failure sg_head must be left
	 * untouched or point to a valid sg_table.
	 */
	int (*dma_map)(struct sg_table *sg_head, void *client_context,
		       struct device *dma_device, int dmasync, int *nmap);
	/**
	 * dma_unmap - Unmap a DMA mapped sg_table
	 *
	 * @sg_head - The sg_table to unmap
	 * @client_context - Value returned by acquire()
	 * @dma_device - The device that will be doing DMA from these addresses
	 *
	 * sg_head will not be touched after this function returns.
	 *
	 * Must return 0.
	 */
	int (*dma_unmap)(struct sg_table *sg_head, void *client_context,
			 struct device *dma_device);
	/**
	 * put_pages - Unpin a SGL
	 *
	 * @sg_head - The sg_table to unpin
	 * @client_context - Value returned by acquire()
	 *
	 * sg_head must be freed on return.
	 */
	void (*put_pages)(struct sg_table *sg_head, void *client_context);
	/* Obsolete, not used */
	unsigned long (*get_page_size)(void *client_context);
	/**
	 * release - Undo acquire
	 *
	 * @client_context - Value returned by acquire()
	 *
	 * If acquire() returns 1 then release() must be called. All
	 * get_pages() and dma_map()'s must be undone before calling this
	 * function.
	 */
	void (*release)(void *client_context);
};

enum {
	PEER_MEM_INVALIDATE_UNMAPS = 1 << 0,
};

struct peer_memory_client_ex {
	struct peer_memory_client client;
	size_t ex_size;
	u32 flags;
};

/*
 * If invalidate_callback() is non-NULL then the client will only support
 * umems which can be invalidated. The caller may call the
 * invalidate_callback() after acquire() on return the range will no longer
 * have DMA active, and release() will have been called.
 *
 * Note: The implementation locking must ensure that get_pages(), and
 * dma_map() do not have locking dependencies with invalidate_callback(). The
 * ib_core will wait until any concurrent get_pages() or dma_map() completes
 * before returning.
 *
 * Similarly, this can call dma_unmap(), put_pages() and release() from within
 * the callback, or will wait for another thread doing those operations to
 * complete.
 *
 * For these reasons the user of invalidate_callback() must be careful with
 * locking.
 */
typedef int (*invalidate_peer_memory)(void *reg_handle, u64 core_context);

void *
ib_register_peer_memory_client(const struct peer_memory_client *peer_client,
	invalidate_peer_memory *invalidate_callback);
void ib_unregister_peer_memory_client(void *reg_handle);
```

## 具体实现过程

在用户程序register mr的时候，IB Core会尝试调用每个client的acquire函数，让每个client判断自己有没有办法将给定的虚拟地址区间翻译成物理页面。在nv_peer_mem中，`nv_mem_acquire()`会尝试调用`nvidia_p2p_get_pages()`来取物理页面，若能成功，就返回True，表示这段内存由我来handle！

IB core找到正确的peer client之后，就会调用其`get_pages()`函数来将虚拟地址区间翻译成物理页面。nv_peer_mem这里会使用`nvidia_p2p_get_pages()`来做地址翻译，并把结果保存在context里。

之后，IB core再调用`dma_map()`来由物理页面map到bus address即dma_address，并将结果保存在sg_head里(sg_head是调用者传入的一个[scatterlist](https://elixir.bootlin.com/linux/v5.4.72/source/include/linux/scatterlist.h)指针， scatterlist专门用来描述一组物理内存)。 nv_peer_mem会使用`nvidia_p2p_dma_map_pages()`。`nvidia_p2p_dma_map_pages()`需要传入peer device，正巧ib_core也会将`dma_device`传入`dma_map()`里。

以上就是装上nv_peer_mem在用户态对一块显存`ibv_reg_mr()`的过程。

### 资源释放

IB卡不再使用这些页面的时候(`dereg_mr`？不确定)，会调用`dma_unmap()`,`put_pages`和`release()`来释放资源。

Peer client可以使用`invalidate_peer_memory()`回调来将一段register过的memory标记为不再dma active。在nv_peer_memory中，由于GPU在做上下文切换的时候有可能会重新分配它的MMIO pages，此时原虚拟地址对应的显存物理页面的映射关系可能失效。在调用`nvidia_p2p_get_pages()`的时候注册了回调`nv_get_p2p_free_callback()`，当映射关系失效时，会调用`nv_get_p2p_free_callback()`，该函数在释放`nvidia_p2p_page_table()`的同时会去调用`invalidate_peer_memory()`告知ib core这段内存不再可用。

有一点需要注意，`nvidia_p2p_get_pages()`在acquire中也被调到了，但那里传入的回调函数是`nv_mem_dummy_callback()`，这个函数其实不太可能会被调到，因为在acquire函数中们get到的page马上就被free了，即使在非常特殊的情况下触发了这个回调，dummy callback也只会与nvidia驱动交互，而不会与IB core通信，这是它与`nv_get_p2p_free_callback()`的区别。

## 参考

1. [How To Implement PeerDirect Client using MLNX_OFED](https://community.mellanox.com/s/article/howto-implement-peerdirect-client-using-mlnx-ofed#jive_content_id_Peer_Memory_RegistrationDeregistration)，这篇文档对某些函数参数的描述已经过时，具体还是参照以上的函数定义。
